import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule, RouterModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  private authService = inject(AuthService)
  router:Router = inject(Router)
  user = new FormGroup({
    email: new FormControl("", [Validators.email, Validators.required]),
    password: new FormControl("", [Validators.required, Validators.minLength(6)]),
    isFilled: new FormControl(false)
  })

  onSubmit() {
    try {
      console.log(this.user.value)
      this.authService.login({ email: this.user.value.email!, password: this.user.value.password! })
      this.router.navigate(["/"])
    } catch (error) {
      alert(error)
    }
  }
  get password() {
    return this.user.get("password")
  }
  get email() {
    return this.user.get("email")
  }
}
