import { Component, inject } from '@angular/core';
import { TitleComponent } from '../../components/title/title.component';
import { InvoiceService } from '../../services/invoice.service';

@Component({
  selector: 'app-history',
  standalone: true,
  imports: [TitleComponent],
  templateUrl: './history.component.html',
  styleUrl: './history.component.css'
})
export class HistoryComponent {
  invoiceService = inject(InvoiceService)

  deleteInvoice(id: number) {
    if (window.confirm(`delete invoice: ${id} ??`)) {
      alert(`Invoice ${id} Deleted`)
    }

  }
}
