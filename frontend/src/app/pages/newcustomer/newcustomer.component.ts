import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormControl, FormControlName, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Customer } from '../../types/CustomerTypes';
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-newcustomer',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule],
  templateUrl: './newcustomer.component.html',
  styleUrl: './newcustomer.component.css'
})
export class NewcustomerComponent {
  adminServic = inject(AdminService)
  initialState = new FormGroup({
    email: new FormControl("", [Validators.email, Validators.required]),
    dni: new FormControl(""),
    id: new FormControl(1),
    name: new FormControl(""),
    address: new FormControl(""),
  })
  newCustomer = this.initialState
  onSubmit() {
    console.log(this.newCustomer.value)
    this.adminServic.createCustomer(this.newCustomer.value as Customer)
    alert("User created")
    this.newCustomer.reset()
  }
}
