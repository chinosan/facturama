import { Component, ElementRef, ViewChild, inject } from '@angular/core';
import { AdminService } from '../../services/admin.service';
import { Customer, Invoice, Product } from '../../types/CustomerTypes'
import { ProductService } from '../../services/product.service';
import { InvoiceService } from '../../services/invoice.service';

@Component({
  selector: 'app-invoices',
  standalone: true,
  imports: [],
  templateUrl: './invoices.component.html',
  styleUrl: './invoices.component.css'
})
export class InvoicesComponent {
  @ViewChild('customerSearchInput') customerInput!: ElementRef<HTMLInputElement>

  public showResults = false;
  public adminService = inject(AdminService)
  public currentCustomer: null | Customer = null
  public customers: Customer[] = this.adminService.customers()

  public productService = inject(ProductService)
  public products = this.productService.products()
  public showProducts = false
  public productsList: ProductList[] = []

  private IVA = 0.16

  private invoiceService = inject(InvoiceService)

  searchCustomer(event: Event) {
    const term = (event.target as HTMLInputElement).value
    console.log(term)
    if (term == "") {
      this.customers = this.adminService.customers()
    }
    this.filterCustomers(term)
  }
  private filterCustomers(term: string) {
    this.customers = this.adminService.customers().filter(c => c.name.toLowerCase().includes(term.toLowerCase()))
  }

  addProduct(product: Product) {
    this.productsList.push({ ...product, quantity: 1 })
  }
  removeProduct(id: number) {
    if (!window.confirm("Delete product?"))
      return
    this.productsList = this.productsList.filter(p => p.id != id)
  }
  increment(e: Event) {
    const { name, value } = e.target as HTMLInputElement
    const finded = this.productsList.find(p => p.id.toString() == name)
    if (finded) {
      if (Number(value) > finded.stock) {
        alert("No hay mas stock")
        return
      }
      finded.quantity = Number(value)
    }
  }
  getSubtotal() {
    return this.productsList.reduce((acc, el) => acc + (el.quantity * el.price), 0)
  }
  getIVA() {
    return this.getSubtotal() * this.IVA
  }
  getTotal() {
    return this.getSubtotal() + this.getIVA()
  }
  onSubmit() {
    if (this.currentCustomer == null) {
      alert("Select a customer")
      return
    }
    if (this.productsList.length == 0) {
      alert("Añada al menos un producto")
      return
    }
    const invoice: Invoice = {
      customer: this.currentCustomer,
      products: this.productsList,
      subtotal: this.getSubtotal(),
      iva: this.getIVA(),
      total: this.getTotal()
    }
    this.invoiceService.createInvoice(invoice)
    this.currentCustomer = null
    this.productsList = []
  }

}
interface ProductList extends Product {
  quantity: number
}
