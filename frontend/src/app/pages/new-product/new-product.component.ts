import { Component, inject } from '@angular/core';
import { FormControl, FormControlName, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Product } from '../../types/CustomerTypes';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-new-product',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './new-product.component.html',
  styleUrl: './new-product.component.css'
})
export class NewProductComponent {
  public productService = inject(ProductService)
  newProduct = new FormGroup({
    id: new FormControl(0),
    code: new FormControl(""),
    name: new FormControl(''),
    price: new FormControl(0),
    stock: new FormControl(0),
    isActive: new FormControl(false),
  })

  handleSubmit() {
    console.log(this.newProduct.value)
    this.productService.createProduct(this.newProduct.value as Product)
  }
}
const p: Product = {
  id: 0,
  code: '',
  name: '',
  price: 0,
  stock: 0,
  isActive: false,
  createdAt: ''
}