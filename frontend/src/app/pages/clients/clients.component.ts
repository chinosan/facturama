import { Component, ElementRef, ViewChild, inject } from '@angular/core';
import { Customer } from '../../types/CustomerTypes';
import { AdminService } from '../../services/admin.service';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-clients',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './clients.component.html',
  styleUrl: './clients.component.css'
})
export class ClientsComponent {
  /*
    public fakeClients: Customer[] = [
      {
        id: '1',
        dni: '123123123',
        name: 'Jose Antonio',
        address: 'Av camelias 2',
        email: 'jose@mail.com',
       // createdAt: new Date()
      }, {
        id: '2',
        dni: '222123123',
        name: 'Lupita Lopex',
        address: 'Calle Reforma 32',
        email: 'lupis@mail.com',
     //   createdAt: new Date()
      }, {
        id: '3',
        dni: '333123123',
        name: 'karla Moreno',
        address: 'Av Siempre Viva 454',
        email: 'karlita@mail.com',
      //  createdAt: new Date()
      },
    ]
    */
  public currentCustomer: Customer = {
    id: 10101010,
    dni: '333123123',
    name: 'karla Moreno',
    address: 'Av Siempre Viva 454',
    email: 'karlita@mail.com',
    createdAt: new Date("2024-01-01")
  }
  adminService = inject(AdminService)

  @ViewChild('dialog')
  dialog!: ElementRef;

  openDialog(customer: Customer) {
    this.currentCustomer = customer
    this.dialog.nativeElement.showModal();
  }

  closeDialog() {
    this.dialog.nativeElement.close();
  }

  deleteCustomer(id: number) {
    this.adminService.deleteCustomer(id)
  }

  onChangeCustomer(e: Event) {
    const { name, value } = e.target as HTMLInputElement
    this.currentCustomer = {...this.currentCustomer, [name]:value}
  }
  editCustomer() {
    this.adminService.updateCustomer(this.currentCustomer)
    //console.log(this.currentCustomer)
    //alert(`edit ${currentcustomer.name}`)
  }

}
