import { Component, inject } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.css'
})
export class SidebarComponent {
  authService = inject(AuthService)
  router: Router = inject(Router)

  routes: { label: string, href: string }[] = [
    { label: "Clients", href: "/" },
    { label: "Products", href: "products" },
    { label: "Invoices", href: "invoices" },
    { label: "History", href: "history" },
  ]
  logout() {
    this.authService.logout()
    this.router.navigate(["/login"])
  }

}
