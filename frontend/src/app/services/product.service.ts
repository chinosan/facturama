import { Injectable, computed, signal } from '@angular/core';
import { Product } from '../types/CustomerTypes';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  #state = signal<State>({
    isLoading: true,
    products: []
  })
  products = computed(() => this.#state().products)
  constructor(private http: HttpClient) {
    this.http.get<Product[]>(PRODUCT_API).subscribe(res => this.#state.set({
      isLoading: false,
      products: res
    }))
  }
  createProduct(product: Product) {
    this.http.post(PRODUCT_API, product).subscribe(res => {
      if (res == null) {
        alert(`no product response`)
      }
      console.log(res)
      alert(`prodcut created`)
      this.#state.set({
        isLoading: false,
        products: [...this.products(), res as Product]
      })
    })
  }
  deleteProduct(id: number) {
    this.http.delete(PRODUCT_API + "/" + id).subscribe(res => {
      if (res == false) {
        alert("product not find")
        return;
      }
      this.#state.set({
        isLoading: false,
        products: this.products().filter(p => p.id != id)
      })
      alert("product deleted")
    })
  }
}



type State = {
  isLoading: boolean,
  products: Product[]
}
const BASE_URL = "http://localhost:8080"
const PRODUCT_API = `${BASE_URL}/api/product`
