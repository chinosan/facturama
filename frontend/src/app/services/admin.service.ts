import { HttpClient } from '@angular/common/http';
import { Injectable, computed, signal } from '@angular/core';
import { Customer } from '../types/CustomerTypes';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  #State = signal<State>({
    isLoading: true,
    customers: []
  })
  public customers = computed(() => this.#State().customers)

  constructor(private http: HttpClient) {
    this.http.get<Customer[]>(CUSTOMER_API).subscribe(res => {
      console.log("fetch customers")
      console.log(res)
      this.#State.set({
        isLoading: false,
        customers: res
      }
      )
    })
  }

  updateCustomer(customer: Customer) {
    this.http.put(CUSTOMER_API, customer).subscribe(res => {
      if (res == null) {
        alert("error update customer")
        return
      }
      this.#State.set({
        isLoading: false,
        customers: this.#State().customers.map(c => c.id == customer.id ? customer : c)
      })
      alert("User updated!!")
    })
  }
  deleteCustomer(id: number) {
    this.http.delete(`${CUSTOMER_API}/${id}`).subscribe(res => {
      this.#State.set({
        isLoading: false,
        customers: this.customers().filter(c => c.id != id)
      })
      alert(`deleted user with ${id}`)
    }, error => {
      console.log(error)
      alert("Error al borrar documento")
    })
  }
  createCustomer(customer: Customer) {
    const newCustomer = {
      dni: customer.dni,
      name: customer.name,
      address: customer.address,
      email: customer.email
    }
    this.http.post(CUSTOMER_API, newCustomer).subscribe(res => {
      if (res == null) {
        console.log("no user returned")
        return;
      }
      console.log(`created user ${customer.name}`)
      console.log(res)
      this.#State.set({
        isLoading: false,
        customers: [...this.customers(), res as Customer]
      })
    })
  }

}

type State = {
  isLoading: boolean,
  customers: Customer[]
}
const BASE_URL = "http://localhost:8080";
const CUSTOMER_API = BASE_URL + "/api/customer";
