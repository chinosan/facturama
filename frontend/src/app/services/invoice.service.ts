import { HttpClient } from '@angular/common/http';
import { Injectable, signal } from '@angular/core';
import { Invoice } from '../types/CustomerTypes';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {
  public invoices = signal<InvoiceResponse[]>([])
  public isLoading = signal(true)

  constructor(private http: HttpClient) {
    this.getAllInvoices()
  }

  createInvoice(data: Invoice) {
    console.log(invoiceDao(data))

    this.http.post(INVOICE_API, invoiceDao(data)).subscribe({
      next: (res) => {
        console.log(res)
        alert(`Created Invoice with id:${(res as any).id}`)
        return res
      },
      error: error => {
        console.log(error)
        alert("Error on create invoice")
      }
    })
  }
  getAllInvoices() {
    this.http.get<InvoiceResponse[]>(INVOICE_API).subscribe({
      next: res => {
        this.invoices.set(res)
        this.isLoading.set(false)
      },
      error: error => {
        console.log(error)
        alert("error fetching data")
        this.isLoading.set(false)
      }
    })
  }
  deleteInvoice(id: number) {
    this.http.delete(`${INVOICE_API}/${id}`).subscribe({
      next: res => {
        console.log(res)
      },
      error: error => {
        console.log(error)
        alert("Erro deleting doc")
      }
    })
  }
}
type InvoiceResponse = {
  "id": number,
  "customerId": number,
  "subtotal": number,
  "total": number,
  "products": {
    "id": number,
    "product_id": number,
    "quantity": number,
    "createdAt": string
  }[],
  "iva": number
}
const response = {
  "id": 152,
  "customerId": 1,
  "subtotal": 30,
  "total": 34,
  "products": [
    {
      "id": 52,
      "product_id": 2,
      "quantity": 22,
      "createdAt": "2024-07-06"
    },
    {
      "id": 53,
      "product_id": 1,
      "quantity": 8,
      "createdAt": "2024-07-06"
    }
  ],
  "iva": 0
}
function invoiceDao(invoice: Invoice) {
  return {
    //id: number,
    customerId: invoice.customer.id,
    iva: invoice.iva,
    subtotal: invoice.subtotal,
    total: invoice.total,
    products: invoice.products.map(p => ({
      //id: number,
      //invoice_id: number,
      product_id: p.id,
      quantity: p.price * p.quantity,
      //createdAt: Date,
      //invoice: Invoice,
    }))
  }
}

type ProductItem = {
  id: number,
  invoice_id: number,
  product_id: number,
  quantity: number,
  //createdAt: Date,
  invoice: Invoice,
}

const INVOICE_API = "http://localhost:8080/api/invoice"
