import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RegisterUser } from '../types/CustomerTypes';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private TOKEN_KEY = "token"
  constructor(private http: HttpClient) { }

  registerUser(newUser: RegisterUser) {
    this.http.post(`${AUTH_API}/signup`, newUser).subscribe({
      next: res => {
        console.log(res)
      },
      error: error => {
        console.log(error)
        alert("Error creating user")
      },
      complete: () => {
        alert("Register successfully")
      }
    })
  }
  login(credentials: { email: string, password: string }) {
    this.http.post<LoginResponse>(`${AUTH_API}/login`, credentials).subscribe({
      next: res => {
        console.log(res)
        localStorage.setItem(this.TOKEN_KEY, res.token)
        // "eyJhbGciOiJIUzI1NiJ9.eyJub21icmUiOiJpdmFuQG1haWwuY29tIiwic3ViIjoiaXZhbkBtYWlsLmNvbSIsImlhdCI6MTcyMTUzMDE0MCwiZXhwIjoxNzIxNTMzNjQwfQ.ZbvaJq2l7Ac7X4C9T8hwRjCfu2WwMEetabdCCj8XG-s"
      },
      error: e => {
        console.log(e)
        alert("error sign in")
      },
      complete: () => {
        alert("Welcome back")
      }
    })
  }
  logout() {
    localStorage.removeItem(this.TOKEN_KEY)
  }

  isLoggedIn() {
    return !!this.getToken()
  }

  getToken() {
    return localStorage.getItem(this.TOKEN_KEY)
  }

}

type LoginResponse = {
  "token": string,
  "expiresIn": number
}

const AUTH_API = "http://localhost:8080/auth"