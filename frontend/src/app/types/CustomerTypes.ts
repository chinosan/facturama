export type Customer = {
  id: number,
  dni: string,
  name: string,
  address: string,
  email: string,
  createdAt?: any
}

export type Product = {
  id: number,
  code: string,
  name: string,
  price: number,
  stock: number,
  isActive: boolean,
  createdAt: Date | string;
}

export type Invoice = {
  customer: Customer,
  products: ProductInvoice[],
  subtotal: number,
  iva: number,
  total: number
}
export type InvoiceItem={
    id: number,
    invoice_id: number,
    product_id: number,
    quantity: number,
    createdAt: Date,
    invoice:Invoice;
}


export type ProductInvoice = Product & {
  quantity:number
}

const n: Invoice = {
  customer: {
    id: 1010,
    dni: "",
    name: "",
    address: "",
    email: "",
    createdAt: undefined
  },
  products: [{
    id: 0,
    code: "asd",
    name: "asd",
    price: 9,
    stock: 0,
    isActive: false,
    createdAt: "2024-03-03",
    quantity:0
  }],
  subtotal: 0,
  iva: 0,
  total: 0
}


export type RegisterUser={
  firstName:String,
  lastName:String,
  email:string,
  password:string
}