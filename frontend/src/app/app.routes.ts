import { Routes } from '@angular/router';
import { ClientsComponent } from './pages/clients/clients.component';
import { ProductsComponent } from './pages/products/products.component';
import { InvoicesComponent } from './pages/invoices/invoices.component';
import { HistoryComponent } from './pages/history/history.component';
import { LoginComponent } from './pages/login/login.component';
import { NewcustomerComponent } from './pages/newcustomer/newcustomer.component';
import { NewProductComponent } from './pages/new-product/new-product.component';
import { RegisterComponent } from './pages/register/register.component';
import { authGuard } from './guards/auth.guard';
import { loginGuard } from './guards/login.guard';

export const routes: Routes = [{
  path: "",
  canActivate: [authGuard],
  component: ClientsComponent,
  title: "Clients",
  children: []
}, {
  path: "products",
  component: ProductsComponent,
  title: "products"
}, {
  path: "invoices",
  component: InvoicesComponent,
  title: "Invoices"
}, {
  path: "history",
  component: HistoryComponent,
  title: "History"
}, {
  path: "login",
  component: LoginComponent,
  title: "Login",
  canActivate: [loginGuard]
}, {
  path: "register",
  component: RegisterComponent,
  title: "Sign Up",
  canActivate: [loginGuard]
}, {
  path: "new",
  component: NewcustomerComponent,
  title: "Add Customer"
}, {
  path: "newproduct",
  component: NewProductComponent,
  title: "Add new Product"
}];
