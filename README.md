# Facturama

A full-stack app for manage invoices

## Frontend - Angular

### Instructions

```
cd frontend
npm install
ng serve
```

## Backend - Spring Boot

```
cd backend
./gradlew.run
```