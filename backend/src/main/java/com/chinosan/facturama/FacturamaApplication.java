package com.chinosan.facturama;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FacturamaApplication {

	public static void main(String[] args) {
		SpringApplication.run(FacturamaApplication.class, args);
	}

}
