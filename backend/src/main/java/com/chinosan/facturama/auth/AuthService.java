package com.chinosan.facturama.auth;

import com.chinosan.facturama.auth.dtos.LoginUserDto;
import com.chinosan.facturama.auth.dtos.RegisterUserDto;
import com.chinosan.facturama.user.User;
import com.chinosan.facturama.user.UserRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    private final UserRepository userRepository;
    private final AuthenticationManager authManager;
    private final PasswordEncoder passwordEncoder;

    public AuthService(UserRepository ur, PasswordEncoder pe, AuthenticationManager am) {
        this.userRepository = ur;
        this.passwordEncoder = pe;
        this.authManager = am;
    }

    public User signUp(RegisterUserDto newUser) {
        User user = new User();
        user.setEmail(newUser.getEmail());
        user.setFirstName(newUser.getFirstName());
        user.setLastName(newUser.getLastName());
        user.setPassword(passwordEncoder.encode(newUser.getPassword()));

        return userRepository.save(user);
    }

    public User authenticate(LoginUserDto user) {
        authManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
        return userRepository.findByEmail(user.getEmail())
                .orElseThrow(null);
    }

}
