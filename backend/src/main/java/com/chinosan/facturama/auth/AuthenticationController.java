package com.chinosan.facturama.auth;

import com.chinosan.facturama.auth.dtos.LoginResponse;
import com.chinosan.facturama.auth.dtos.LoginUserDto;
import com.chinosan.facturama.auth.dtos.RegisterUserDto;
import com.chinosan.facturama.jwt.JwtService;
import com.chinosan.facturama.user.SecurityUser;
import com.chinosan.facturama.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "http://localhost:4200")
public class AuthenticationController {
    @Autowired
    private AuthService authService;
    @Autowired
    private JwtService jwtService;

    @GetMapping
    public String health() {
        return "This api is working fine";
    }

    @PostMapping("/signup")
    public ResponseEntity<User> register(@RequestBody RegisterUserDto newUser) {
        User registerUser = authService.signUp(newUser);
        return ResponseEntity.ok(registerUser);
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginUserDto loginUser) {
        User authenticatedUser = authService.authenticate(loginUser);
        String token = jwtService.generateToken(new SecurityUser(authenticatedUser));
        LoginResponse response = new LoginResponse();
        response.setToken(token);
        response.setExpiresIn(jwtService.getExpirationTime());
        return ResponseEntity.ok(response);
    }
}
