package com.chinosan.facturama.publc;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PublicController {

    @GetMapping("/public")
    public String greetings() {
        return "Hello, this endpoint its public";
    }

    @GetMapping("/admin")
    public String authUsers() {
        return "This endpoint its for authenticated users";
    }

    @GetMapping("/admin/role")
    @PreAuthorize("hasAuthority('ADMIN')")
   // @PreAuthorize("hasRole('Admin')")
    public String adminRole() {
        return "Just ADmin users can see this";
    }
}
