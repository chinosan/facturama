package com.chinosan.facturama.user;

public enum UserRoles {
    USER,
    ADMIN,
    VISITOR
}
