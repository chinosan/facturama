package com.chinosan.facturama.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public List<Customer> getAll() {
        return customerRepository.findAll();
    }

    public Customer addCustomer(Customer customer) {
        return this.customerRepository.save(customer);
    }

    public Customer updateCustomer(Customer customer) {
        return this.customerRepository.save(customer);
    }
    public void removeCustomer(int customerId){
        this.customerRepository.deleteById(customerId);
    }
}

class CustomerNotFound extends Exception{
    public CustomerNotFound(String e){
        super(e);
    }
}