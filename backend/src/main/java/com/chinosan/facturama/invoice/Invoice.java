package com.chinosan.facturama.invoice;

import com.chinosan.facturama.invoice.invoiceItem.InvoiceItem;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.Objects;
import java.util.Set;

@Entity
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int customerId;
    private int IVA;
    private int subtotal;
    private int total;
    @JsonManagedReference
    @OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<InvoiceItem> products;

    public Invoice() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getIVA() {
        return IVA;
    }

    public void setIVA(int IVA) {
        this.IVA = IVA;
    }

    public int getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(int subtotal) {
        this.subtotal = subtotal;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
/*
    public Set<InvoiceItem> getProducts() {
        return products;
    }

    public void setProducts(Set<InvoiceItem> products) {
        this.products = products;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invoice invoice = (Invoice) o;
        return customerId == invoice.customerId && IVA == invoice.IVA && subtotal == invoice.subtotal && total == invoice.total && Objects.equals(id, invoice.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customerId, IVA, subtotal, total);
    }
}

/*
const n: Invoice = {
customer: {
id: "",
dni: "",
name: "",
address: "",
email: "",
createdAt: undefined
  },
products: [{
id: 0,
code: "asd",
name: "asd",
price: 9,
stock: 0,
isActive: false,
createdAt: "2024-03-03",
quantity:0
        }],
subtotal: 0,
iva: 0,
total: 0
        }
        */