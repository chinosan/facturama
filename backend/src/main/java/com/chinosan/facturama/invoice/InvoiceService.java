package com.chinosan.facturama.invoice;

import com.chinosan.facturama.invoice.invoiceItem.InvoiceItem;
import com.chinosan.facturama.invoice.invoiceItem.InvoiceItemService;
import com.chinosan.facturama.product.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class InvoiceService {
    @Autowired
    private InvoiceRepository invoiceRepository;

    public Invoice createinvoice(Invoice invoice) {
        return this.invoiceRepository.save(invoice);
    }

    public List<Invoice> getAllInvoice() {
        return this.invoiceRepository.findAll();
    }
    public void deleteInvoice(Long id){
        this.invoiceRepository.deleteById(id);
    }
}
