package com.chinosan.facturama.invoice.invoiceItem;

import com.chinosan.facturama.invoice.Invoice;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.Objects;

@Entity
public class InvoiceItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    //  private Long invoice_id;
    private Long product_id;
    private double quantity;
    private LocalDate createdAt;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(
            name = "invoice_id"
            //        nullable = false, insertable = false, updatable = false
    )
    private Invoice invoice;

    public InvoiceItem() {
        this.createdAt = LocalDate.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
/*
    public Long getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(Long invoice_id) {
        this.invoice_id = invoice_id;
    }*/

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvoiceItem that = (InvoiceItem) o;
        return Double.compare(quantity, that.quantity) == 0 && Objects.equals(id, that.id) && Objects.equals(product_id, that.product_id) && Objects.equals(createdAt, that.createdAt) && Objects.equals(invoice, that.invoice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, product_id, quantity, createdAt, invoice);
    }
}
