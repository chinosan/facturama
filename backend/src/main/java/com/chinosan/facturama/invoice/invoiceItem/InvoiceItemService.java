package com.chinosan.facturama.invoice.invoiceItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class InvoiceItemService {
    @Autowired
    private InvoiceItemRepository invoiceItemRepository;

    public Set<InvoiceItem> addItems(Set<InvoiceItem> items, Long invoiceId) {
        Set<InvoiceItem> result = new HashSet<>();
        for (InvoiceItem item : items) {
           // item.setInvoice_id(invoiceId);
            result.add(invoiceItemRepository.save(item));
        }
        return result;
    }
}
